/**
 * Creates a get version response {@link Message}.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appgenerics;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppGenericsGetAttributeResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppGenericsGetAttributeResponse.class.getSimpleName();

  /**
   * Used to register observer against this message Id.
   * 
   * @param endpoint Endpoint.
   * @return Registration id of a AppGenericsGetVersionResponse..
   */
  public static final int getRegistrationId(byte endpoint) {
    return MessageFields.createId(new byte[] {0, 0, MessageFields.SOF,
        MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_APP, MessageFields.APP_CLUSTER_GENERICS,
        AppGenerics.GEN_CMD_READ, endpoint, 0, 0});
  }

  /**
   * The plc status bitfields.
   */
  private final int status;

  /**
   * Attribute cluster id.
   */
  private final byte clusterId;

  /**
   * Attribute id.
   */
  private final byte attributeId;
  
  /**
   * Attribute len
   */
  private final byte attributeLen;
  
  /**
   * Attribute datatype
   */
  private final byte attributeDataType;

  /**
   * Attribute data in little endian.
   */
  private final byte[] attributeData;

  /**
   * The sequence number of this message.
   */
  private final byte sequenceNumber;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public AppGenericsGetAttributeResponse(InetAddress address, byte[] data)
      throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 12);
    this.sequenceNumber = MessageFields.getSequenceNumber(data);
    this.clusterId = data[8];
    this.attributeId = data[9];
    this.attributeDataType = data[10];
    this.attributeLen = data[11];
//    int attributeLen = data.length - 18;
    this.attributeData = new byte[attributeLen];
    Helpers.arrayToArray(data, attributeData, 16, attributeLen, 0);
  }

  /**
   * Don't allow.
   */
  private AppGenericsGetAttributeResponse() {
    super(null, null);
    this.status = 0;
    this.sequenceNumber = 0;
    this.clusterId = 0;
    this.attributeId = 0;
    this.attributeDataType = 0;
    this.attributeLen = 0;
    this.attributeData = null;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof AppGenerics) {
      ((AppGenerics) obs).onAppGenericsGetAttributeResponse(super.address, this.status,
          MessageFields.getEndpoint(super.data), this.clusterId, this.attributeId, this.attributeDataType,
          this.attributeData, this.sequenceNumber);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  @Override
  public String toString() {
    return LOG_TAG + ": "
        + super.toString()
        + MessageDebugHelpers.printEndpoint(super.data)
        + MessageDebugHelpers.printStatus(this.status)
        + " clusterId(" + Integer.toString(clusterId)
        + "), attributeId(" + Integer.toString(attributeId)
        + "), attributeDatatype(" + Integer.toString(attributeDataType)
        + "), attributeDataLen(" + Integer.toString(attributeLen)
        + "), attributeData("+ Helpers.arrayToHexString(attributeData, attributeData.length)
        + "), " + MessageDebugHelpers.printSequenceNumber(super.data)
        ;
  }
}
