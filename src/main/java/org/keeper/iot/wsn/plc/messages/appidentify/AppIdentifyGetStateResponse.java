/**
 * Creates a response {@link Message} from raw data.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appidentify;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppIdentifyGetStateResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppIdentifyGetStateResponse.class.getSimpleName();

  /**
   * Used to register observer against this message Id.
   * 
   * @param endpoint Endpoint.
   * @return Registration id of a AppGenericsGetVersionResponse..
   */
  public static final int getRegistrationId(byte endpoint) {
    return MessageFields.createId(new byte[] {0, 0, MessageFields.SOF,
        MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_APP, MessageFields.APP_CLUSTER_IDENTIFY,
        AppIdentify.IDENT_CMD_GET_STATE, endpoint, 0, 0});
  }

  /**
   * The PLC status bit-fields.
   */
  private final int status;

  /**
   * The identify state.
   */
  private final byte state;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public AppIdentifyGetStateResponse(InetAddress address, byte[] data)
      throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 7);
    this.state = data[11];
  }

  /**
   * Don't allow.
   */
  private AppIdentifyGetStateResponse() {
    super(null, null);
    this.status = 0;
    this.state = 0;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof AppIdentify) {
      ((AppIdentify) obs).onAppIdentifyGetStateResponse(super.address,
          MessageFields.getEndpoint(super.data), this.status, this.state,
          MessageFields.getSequenceNumber(data));
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return LOG_TAG + ": "
        + MessageDebugHelpers.printEndpoint(super.data)
        + MessageDebugHelpers.printStatus(this.status) 
        + " State(" + Helpers.byteToHexString(this.state) + ") "
        + MessageDebugHelpers.printSequenceNumber(super.data)
        + super.toString();
  }
}
