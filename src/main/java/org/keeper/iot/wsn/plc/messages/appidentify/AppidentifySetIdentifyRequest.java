/**
 * Creates a get version request {@link Message}.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appidentify;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.SequenceNumber;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppidentifySetIdentifyRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppidentifySetIdentifyRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * @param endpoint Target endpoint.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address, byte endpoint, byte duration) {
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_APP,
            MessageFields.APP_CLUSTER_IDENTIFY, AppIdentify.IDENT_CMD_IDENTIFY, endpoint, duration,
            (byte) SequenceNumber.getSequenceNumber(), 0};
    Message message =
        new AppidentifySetIdentifyRequest(address, messageData,
            AppIdentifySetIdentifyResponse.getRegistrationId(endpoint));

    return message;
  }

  /**
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private AppidentifySetIdentifyRequest(InetAddress address, byte[] data, int responseId) {
    super(address, data, responseId);
  }

  private byte getDuration() {
    return super.data[7];
  }
  
  /**
   * Description of message.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  public String toString() {
    return LOG_TAG + ": "
        + super.toString()
        + MessageDebugHelpers.printCmdClusterId(super.data)
        + MessageDebugHelpers.printEndpoint(super.data)
        + ", Duration(" + Integer.toString(getDuration())
        + MessageDebugHelpers.printSequenceNumber(super.data);
  }
}
