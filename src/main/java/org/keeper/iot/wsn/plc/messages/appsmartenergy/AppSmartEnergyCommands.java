package org.keeper.iot.wsn.plc.messages.appsmartenergy;

public class AppSmartEnergyCommands {
  public static final byte SE_CMD_GET_VERSION = 0;
  public static final byte SE_CMD_SET_OP_MODE = 1;
  public static final byte SE_CMD_GET_OP_MODE = 2;
  public static final byte SE_CMD_SET_OP_MODE_AND_GAIN_FACTOR = 3;
  public static final byte SE_CMD_GET_OP_MODE_AND_GAIN_FACTOR = 4;
  
  public static final byte SE_OP_MODE_0 = 0;
  public static final byte SE_OP_MODE_1 = 1;
  public static final byte SE_OP_MODE_2 = 2;
}
