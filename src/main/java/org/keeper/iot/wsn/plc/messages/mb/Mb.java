package org.keeper.iot.wsn.plc.messages.mb;

import java.net.InetAddress;

public interface Mb {
  public static final int MB_MAX_MSG_LENGTH = 250;
  
  /**
   * Cmd1 value to get subsystem version.
   */
  public static final byte MB_GET_VERSION = (0);
  /**
   * Cmd1 value to enable modbus.
   */
  public static final byte MB_ENABLE = (1);
  /**
   * Cmd1 value to send/receive modbus data.
   */
  public static final byte MB_COM_DATA = (2);
  /**
   * Cmd1 value to get modbus status.
   */
  public static final byte MB_GET_STATUS = (3);

  /**
   * Response with subsystem version.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param version Version of this subsystem.
   */
  public void onMbGetVersionResponse(InetAddress address, int status, byte version);

  /**
   * Mb enable response.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   */
  public void onMbSetEnableResponse(InetAddress address, int status);
  
  /**
   * Mb send data response.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   */
  public void onMbSendDataResponse(InetAddress address, int status);
  
  /**
   * Mb receive data request from device.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param data Modbus data.
   */
  public void onMbReceiveDataRequest(InetAddress address, int status, byte[] data);
  
  /**
   * Mb get status response.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   */
  public void onMbGetStatusResponse(InetAddress address, int status);
}
