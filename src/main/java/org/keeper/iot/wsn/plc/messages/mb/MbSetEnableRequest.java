/**
 * Creates a modbus set enable request {@link Message} from parameters.
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.mb;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;

import java.net.InetAddress;

/**
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class MbSetEnableRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = MbSetEnableRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address) {
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_MB,
            Mb.MB_ENABLE, 0};
    Message message = new MbSetEnableRequest(address, messageData);

    return message;
  }

  /**
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private MbSetEnableRequest(InetAddress address, byte[] data) {
    super(address, data, MbSetEnableResponse.REGISTRATION_ID);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  /**
   * Method toString.
   * 
   * @return String
   */
  public String toString() {
    return LOG_TAG + super.toString();
  }
}
