/**
 * Interface and definitions for the Nwk subsystem.
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.nwk;

import org.keeper.iot.wsn.plc.messages.DataTypes;

import java.net.InetAddress;

/**
 * Interface and attributes for the Nwk subsystem.
 * 
 * @author sergio.soldado@withus.pt
 *
 */
public interface Nwk {
  /**
   * Cmd1 value to get subsystem version.
   */
  public static final byte NWK_GET_VERSION = (0);
  /**
   * Cmd1 value to confirm device access.
   */
  public static final byte NWK_ANNOUNCE = (1);
  /**
   * Cmd1 value to set plc network key.
   */
  public static final byte NWK_SET_KEY = (2);
  /**
   * Cmd1 value to instruct device to leave network.
   */
  public static final byte NWK_LEAVE = (3);
  /**
   * Cmd1 value to configure device sleep mode.
   */
  public static final byte NWK_DEVICE_SLEEP_MODE = (4);
  /**
   * Cmd1 value for keep-alive confirmation.
   */
  public static final byte NWK_KEEPALIVE = (5);
  /**
   * Cmd1 value to configure keep-alive messages.
   */
  public static final byte NWK_KEEPALIVE_CFG = (6);
  public static final byte NWK_KEEPALIVE_CFG_TIMER0_DATATYPE = DataTypes.DATATYPE_UINT16_ID;
  public static final byte NWK_KEEPALIVE_CFG_TIMER0_LENGTH = DataTypes.DATATYPE_UINT16_SIZE;
  public static final byte NWK_KEEPALIVE_CFG_TIMER1_DATATYPE = DataTypes.DATATYPE_UINT16_ID;
  public static final byte NWK_KEEPALIVE_CFG_TIMER1_LENGTH = DataTypes.DATATYPE_UINT16_SIZE;
  public static final byte NWK_KEEPALIVE_CFG_CFG_DATATYPE = DataTypes.DATATYPE_UINT8_ID;
  public static final byte NWK_KEEPALIVE_CFG_CFG_LENGTH = DataTypes.DATATYPE_UINT8_SIZE;
  
  // TODO Configuration values
//  NWK_KEEPALIVE_CFG_RESET_PLC     = (0),
//      NWK_KEEPALIVE_CFG_DHCP          = (1),
//      NWK_KEEPALIVE_CFG_REBOOT        = (2),
  /**
   * N/A.
   */
  public static final byte NWK_INVALID = (10); // for testing

  /**
   * Response with Nwk subsystem version if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param version Version of this subsystem.
   */
  public void onNwkGetVersionResponse(InetAddress address, int status, byte version);

  /**
   * Announce request from device.
   * 
   * @param address Address of device that originated this message.
   */
  public void onNwkAnnounceRequest(InetAddress address);

  /**
   * Device announce response.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   */
  public void onNwkAnnounceResponse(InetAddress address, int status);

  /**
   * Device leave response.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   */
  public void onNwkLeaveResponse(InetAddress address, int status);

  /**
   * Keepalive request from device.
   * 
   * @param address Address of device that originated this message.
   */
  public void onNwkKeepaliveRequest(InetAddress address);

  /**
   * Device keep-alive configuration response.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   */
  public void onNwkKeepaliveConfigureResponse(InetAddress address, int status);

}
