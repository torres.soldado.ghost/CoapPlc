/**
 * Creates a network device leave reqest {@link Message} from parameters.
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.nwk;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;

import java.net.InetAddress;

/**
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class NwkDeviceLeaveRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = NwkDeviceLeaveRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address) {
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_NWK,
            Nwk.NWK_LEAVE, 0};
    Message message = new NwkDeviceLeaveRequest(address, messageData);

    return message;
  }

  /**
   * SysRebootRequest.
   * 
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private NwkDeviceLeaveRequest(InetAddress address, byte[] data) {
    super(address, data, NwkDeviceLeaveResponse.REGISTRATION_ID);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  public String toString() {
    return LOG_TAG + ": " + super.toString();
  }
}
