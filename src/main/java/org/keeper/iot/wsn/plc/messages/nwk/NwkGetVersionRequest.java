/**
 * NwkGetVersionRequest.java
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.nwk;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class NwkGetVersionRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = NwkGetVersionRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address) {
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_NWK,
            Nwk.NWK_GET_VERSION, 0};
    Message message = new NwkGetVersionRequest(address, messageData);

    return message;
  }

  /**
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private NwkGetVersionRequest(InetAddress address, byte[] data) {
    super(address, data, NwkGetVersionResponse.REGISTRATION_ID);
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  public String toString() {
    return LOG_TAG + ": " + super.toString();
  }
}
