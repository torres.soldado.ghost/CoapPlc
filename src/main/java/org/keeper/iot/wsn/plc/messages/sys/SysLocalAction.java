package org.keeper.iot.wsn.plc.messages.sys;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class SysLocalAction {
  public static final int SYS_LOCAL_ACTION_REBOOT = 0;
  public static final int SYS_LOCAL_ACTION_JOIN = 1;
  public static final int SYS_LOCAL_ACTION_LEAVE = 2;
  public static final int SYS_LOCAL_ACTION_MODE1 = 3;
  public static final int SYS_LOCAL_ACTION_MODE2 = 4;
  public static final int SYS_LOCAL_ACTION_MODE3 = 5;
  public static final int SYS_LOCAL_ACTION_IDENTIFY = 6;
  public static final int SYS_LOCAL_ACTION_FACTORY_RESET = 7;

  private static final Map<Integer, String> SYS_LOCAL_ACTION_MAP;

  static {
    final Map<Integer, String> map = new HashMap<Integer, String>(16, 1.0f);
    map.put(0, "SYS_LOCAL_ACTION_REBOOT");
    map.put(1, "SYS_LOCAL_ACTION_JOIN");
    map.put(2, "SYS_LOCAL_ACTION_LEAVE");
    map.put(3, "SYS_LOCAL_ACTION_MODE1");
    map.put(4, "SYS_LOCAL_ACTION_MODE2");
    map.put(5, "SYS_LOCAL_ACTION_MODE3");
    map.put(6, "SYS_LOCAL_ACTION_IDENTIFY");
    map.put(7, "SYS_LOCAL_ACTION_FACTORY_RESET");

    SYS_LOCAL_ACTION_MAP = Collections.unmodifiableMap(map);
  }

  /**
   * Gets string with textual representation of user actions.
   * @param localAction From message.
   * @return Textual representation.
   */
  public static String getLocalActionStr(byte localAction) {
    final StringBuilder sb = new StringBuilder(512);
    for (int i = 0; i < 8; ++i) {
      if (0 != (localAction & (1 << i))) {
        sb.append(SYS_LOCAL_ACTION_MAP.get(Integer.valueOf(i)));
      }
    }

    return sb.toString();
  }
}
