package org.keeper.iot.wsn.plc.observer;

import java.net.InetAddress;

/**
 * Message broker interface.
 */
public interface IBroker {
  /**
   * Registers the observer with the given parameters.
   * The observer remains registers until it is explicitly unregistered.
   * 
   * @param obs Observer to register.
   * @param address Address of device to subscribe to.
   * @param messageId Message id to subscribe to.
   * @param oneShot True to unregister observer automatically after message/timeout is received.
   */
  public void registerObserver(IObserver obs, InetAddress address, int messageId, boolean oneShot);

  /**
   * Registers the observer with the given parameters.
   * The observer remains registers until it is explicitly unregistered.
   * 
   * @param obs Observer to register.
   * @param messageId Message id to subscribe to.
   * @param oneShot True to unregister observer automatically
   */
  public void registerObserver(IObserver obs, int messageId, boolean oneShot);

  /**
   * Unregisters the observer, i.e. it won't receive any further notifications.
   * 
   * @param obs Observer to register.
   * @param address Address of device to subscribe to.
   * @param messageId Message id to subscribe to.
   */
  public void removeObserver(IObserver obs, InetAddress address, int messageId);

  /**
   * Unregisters the observer, i.e. it won't receive any further notifications.
   * 
   * @param obs Observer to register.
   * @param messageId Message id to subscribe to.
   */
  public void removeObserver(IObserver obs, int messageId);

}
