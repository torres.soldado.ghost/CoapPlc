package org.keeper.iot.wsn.plc.resources;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics;
import org.keeper.iot.wsn.plc.observer.IObserver;

import java.net.InetAddress;

/**
 * Since this is a generic cluster the implementation doesn't contain any resource tree. Therefore
 * this class should first be extended into an additional encapsulated class and the relevant
 * methods overridden.
 * 
 * @author sergio.soldado@withus.pt
 */
public class AppGenericsHandler implements IObserver,
    AppGenerics {
  /**
   * Used by logger
   */
  private final String tag;
  /**
   * This class is designed to be encapsulated. We get some fields from the parent class since they
   * generally implement the same interface.
   */
  private IResourceManager parent;

  /**
   * @param parent Parent {@link IResourceHandler}
   */
  public AppGenericsHandler(IResourceManager parent, String tag) {
    this.parent = parent;
    this.tag = tag;
  }

  @Override
  public void onAppGenericsGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber) {
    MessageDebugHelpers.onUnhandledMessage(tag, "onAppGenericsGetVersionResponse");
  }

  @Override
  public void onAppGenericsGetAttributeResponse(InetAddress address, int status, byte endpoint,
      byte clusterId, byte attributeId, byte datatype, byte[] attributeData, byte sequenceNumber) {
    MessageDebugHelpers.onUnhandledMessage(tag, "onAppGenericsGetAttributeResponse");
  }

  @Override
  public void onAppGenericsSetAttributeResponse(InetAddress address, int status, byte clusterId,
      byte attributeId, byte sequenceNum) {
    MessageDebugHelpers.onUnhandledMessage(tag, "onAppGenericsSetAttributeResponse");
  }

  @Override
  public void onAppGenericsPeriodicReportConfigureResponse(InetAddress address, byte endpoint,
      int status, byte clusterId, byte attributeId, byte sequenceNum) {
    MessageDebugHelpers.onUnhandledMessage(tag, "onAppGenericsPeriodicReportConfigureResponse");
  }

  @Override
  public void onAppGenericsPeriodicReport(InetAddress address, byte endpoint, int status,
      byte clusterId, byte attributeId, byte[] attributeData, byte sequenceNumber) {
    MessageDebugHelpers.onUnhandledMessage(tag, "onAppGenericsPeriodicReport");
  }

  @Override
  public void onAppGenericsAlertConfigureResponse(InetAddress address, byte endpoint, int status,
      byte clusterId, byte attributeId, byte sequenceNum) {
    MessageDebugHelpers.onUnhandledMessage(tag, "onAppGenericsAlertConfigureResponse");
  }

  @Override
  public void onAppGenericsRangeAlert(InetAddress address, byte endpoint, int status,
      byte clusterId, byte attributeId, byte[] attributeData, byte alertType) {
    MessageDebugHelpers.onUnhandledMessage(tag, "onAppGenericsRangeAlert");
  }

  @Override
  public void onAppGenericsCapabilities(InetAddress address, int status, Capabilities capabilities) {
    MessageDebugHelpers.onUnhandledMessage(tag, "onAppGenericsCapabilities");
  }

  @Override
  public void onDeviceMessage(Message msg) {
    if (MessageDebugHelpers.isSameAddresses(tag, msg.getAddress(), this.parent.getAddress())) {
      msg.callHandler(this);
    }
  }

  @Override
  public void onDeviceMessageTimeout(int msgId) {
    MessageDebugHelpers.debugOnMessageTimeout(tag, parent.getAddress(), msgId);
  }

}
