package org.keeper.iot.wsn.plc.resources;

import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.appsmarttemp.*;
import org.keeper.iot.wsn.plc.observer.IObserver;

import java.net.InetAddress;
import java.util.List;

public class AppSmartTempResourceManager implements IResourceManager, IObserver, AppSmarttemp {
  /**
   * Used by logger.
   */
  private static final String TAG = AppOnOffResourceManager.class.getSimpleName();

  /**
   * The target device's address.
   */

  private final InetAddress address;
  /**
   * The target endpoint.
   */
  private final byte endpoint;

  private final PlcResource currentTemp;
  private final PlcResource maxTemp;
  private final PlcResource minTemp;
  private final PlcResource resVersion;
  private final PlcResource root;

  public AppSmartTempResourceManager(ICore core, InetAddress address, byte endpoint) {
    this.address = address;
    this.endpoint = endpoint;
    root = new PlcResource("SmartTemp");

    resVersion = new PlcResource("Version") {
      @Override
      public void remoteGet() {
        core.send(AppSmartTempGetVersionRequest.createMessage(address, endpoint));
      }
    };
    core.subscribe(this, address, AppSmartTempGetVersionResponse.getRegistrationId(endpoint));
    root.addChild(resVersion);

    currentTemp = new PlcResource("CurrentTemperature") {
      @Override
      public void remoteGet() {
        core.send(AppSmartTempGetCurTempRequest.createMessage(address, endpoint));
      }
    };
    core.subscribe(this, address, AppSmartTempGetCurTempResponse.getRegistrationId(endpoint));
    root.addChild(currentTemp);

    maxTemp = new PlcResource("MaxTemperature") {
      @Override
      public void remoteGet() {
        core.send(AppSmartTempGetMaxTempRequest.createMessage(address, endpoint));
      }
    };
    core.subscribe(this, address, AppSmartTempGetMaxTempResponse.getRegistrationId(endpoint));
    root.addChild(maxTemp);

    minTemp = new PlcResource("MinTemperature") {
      @Override
      public void remoteGet() {
        core.send(AppSmartTempGetMinTempRequest.createMessage(address, endpoint));
      }
    };
    core.subscribe(this, address, AppSmartTempGetMinTempResponse.getRegistrationId(endpoint));
    root.addChild(minTemp);
  }

  @Override
  public void onDeviceMessage(Message msg) {
    if (MessageDebugHelpers.isSameAddresses(TAG, this.address, msg.getAddress())) {
      msg.callHandler(this);
    }
  }

  @Override
  public void onDeviceMessageTimeout(int msgId) {
    MessageDebugHelpers.debugOnMessageTimeout(TAG, address, msgId);
  }

  @Override
  public InetAddress getAddress() {
    return address;
  }

  @Override
  public PlcResource getChild(String[] path) {
    return root.getChild(path, 0);
  }

  @Override
  public PlcResource getRoot() {
    return root;
  }

  @Override
  public void remoteSetAsync(String path, List<String> parameters) {
    MessageDebugHelpers.setResource(TAG, path, parameters, root);
  }

  @Override
  public void remoteUpdateAll() {
    root.remoteGetAll();
  }

  @Override
  public void onAppSmarttempGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber) {
    if (MessageDebugHelpers.isMessageForMe(TAG, this.endpoint, endpoint, status)) {
      resVersion.setValue(String.valueOf(version));
    }
  }

  @Override
  public void onAppSmarttempGetCurTempResponse(InetAddress address, byte endpoint, int status,
      byte temperature, byte sequenceNumber) {
    if (MessageDebugHelpers.isMessageForMeAndValidStatus(TAG, this.endpoint, endpoint, status)) {
      currentTemp.setValue(String.valueOf(temperature) + "°C");
    }
  }

  @Override
  public void onAppSmarttempGetMaxTempResponse(InetAddress address, byte endpoint, int status,
      byte temperature, byte sequenceNumber) {
    if (MessageDebugHelpers.isMessageForMeAndValidStatus(TAG, this.endpoint, endpoint, status)) {
      maxTemp.setValue(String.valueOf(temperature) + "°C");
    }
  }

  @Override
  public void onAppSmarttempGetMinTempResponse(InetAddress address, byte endpoint, int status,
      byte temperature, byte sequenceNumber) {
    if (MessageDebugHelpers.isMessageForMeAndValidStatus(TAG, this.endpoint, endpoint, status)) {
      minTemp.setValue(String.valueOf(temperature) + "°C");
    }
  }

}
