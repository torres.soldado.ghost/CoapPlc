package org.keeper.iot.wsn.plc.resources;

import java.net.InetAddress;
import java.util.List;


public interface IResourceManager {
  
  public InetAddress getAddress();
  
  /**
   * Gets a given child resource node.
   * 
   * @param path Resource URI path, e.g. a list form of "/Endpoint0/AppOnOff/clusterVersion" which
   * would be a list composed of { "Endpoint0", "AppOnOff", "clusterVersion"}. 
   * @return PlcResource if child node exists, null otherwise.
   */
  public PlcResource getChild(String[] path);
  
  /**
   * Returns the root resource node. This node contains all resource nodes.
   * @return root resource node if it exists, null otherwise.
   */
  public PlcResource getRoot();

  /**
   * Execute some action on a given resource.
   * 
   * @param address the target device's address.
   * @param contractParameters Data necessary to perform action, e.g. to toggle a relay the state
   * value would be necessary so an example would be relay.remoteSet(someAddress, {"State", "ON"});
   * @return true if successful, false otherwise.
   */
  public void remoteSetAsync(String path, List<String> parameters);

  /**
   * Sends message to remote device to retrieve resource value.
   * @param address the target device's address.
   */
  public void remoteUpdateAll();

}
