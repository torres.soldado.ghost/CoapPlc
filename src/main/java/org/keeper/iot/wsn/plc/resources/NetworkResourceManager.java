package org.keeper.iot.wsn.plc.resources;

import static com.esotericsoftware.minlog.Log.*;

import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageStatus;
import org.keeper.iot.wsn.plc.messages.nwk.*;
import org.keeper.iot.wsn.plc.observer.IObserver;

import java.net.InetAddress;
import java.util.Date;
import java.util.List;

public class NetworkResourceManager implements IResourceManager, IObserver, Nwk {
  /**
   * Used by logger.
   */
  private final String TAG;

  /**
   * The target device's address.
   */
  private final InetAddress address;

  private final PlcResource root;
  private final PlcResource resVersion;
  private final PlcResource resCommands;
  private final PlcResource resStatus;

  public NetworkResourceManager(ICore core, InetAddress address) {
    this.TAG = this.getClass().getSimpleName() + "@" + address.getHostAddress();
    this.address = address;
    root = new PlcResource("Network");

    resVersion = new PlcResource("Version") {
      @Override
      public void remoteGet() {
        core.send(NwkGetVersionRequest.createMessage(address));
      }
    };
    core.subscribe(this, address, NwkGetVersionResponse.REGISTRATION_ID);
    root.addChild(resVersion);

    resStatus = new PlcResource("Status", "Unknown");
    root.addChild(resStatus);

    resCommands = new PlcResource("Commands", "Supported commands: leave, keepaliveCfg") {
      @Override
      public void remoteSet(List<String> parameters) {
        if (null == parameters || 0 == parameters.size()) {
          warn(TAG, "Empty set command");
          return;
        }

        String command = parameters.get(0);
        if (command.equalsIgnoreCase("leave")) {
          core.send(NwkDeviceLeaveRequest.createMessage(address));
          info("Issued device leave for " + address.getHostAddress());
        } else if (command.equalsIgnoreCase("keepaliveCfg")) {
          if (4 != parameters.size()) {
            warn(TAG, "Invalid parameters for " + getName());
            return;
          }

          byte[] timer0 =
              ResourceHelpers.resourceStringToRaw(Nwk.NWK_KEEPALIVE_CFG_TIMER0_DATATYPE,
                  parameters.get(1), Nwk.NWK_KEEPALIVE_CFG_TIMER0_LENGTH);
          byte[] timer1 =
              ResourceHelpers.resourceStringToRaw(Nwk.NWK_KEEPALIVE_CFG_TIMER1_DATATYPE,
                  parameters.get(2), Nwk.NWK_KEEPALIVE_CFG_TIMER1_LENGTH);
          byte[] cfg =
              ResourceHelpers.resourceStringToRaw(Nwk.NWK_KEEPALIVE_CFG_CFG_DATATYPE,
                  parameters.get(3), Nwk.NWK_KEEPALIVE_CFG_CFG_LENGTH);

          core.send(NwkKeepaliveConfigurationRequest.createMessage(address, timer0, timer1, cfg[0]));
          info(TAG,
              "keepalive cfg with timer0(" + parameters.get(1) + "), timer1(" + parameters.get(2)
                  + ", cfg(" + parameters.get(3) + ")");
        }
      }
    };
    root.addChild(resCommands);

    core.subscribe(this, address, NwkDeviceLeaveResponse.REGISTRATION_ID);
    core.subscribe(this, address, NwkKeepaliveDeviceRequest.REGISTRATION_ID);
    core.subscribe(this, address, NwkKeepaliveConfigurationResponse.REGISTRATION_ID);
  }

  @Override
  public void onNwkGetVersionResponse(InetAddress address, int status, byte version) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      resVersion.setValue(String.valueOf(version));
    }
  }

  @Override
  public void onNwkAnnounceRequest(InetAddress address) {}

  @Override
  public void onNwkAnnounceResponse(InetAddress address, int status) {}

  @Override
  public void onNwkLeaveResponse(InetAddress address, int status) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      info(TAG, "Device left network");
      resStatus.setValue("Left Network");
    }
  }

  @Override
  public void onNwkKeepaliveRequest(InetAddress address) {
    resStatus.setValue("Last seen alive at " + new Date().toString());
  }

  @Override
  public void onNwkKeepaliveConfigureResponse(InetAddress address, int status) {
    MessageStatus.isStatusSuccessfull(TAG, status);
  }

  @Override
  public void onDeviceMessage(Message msg) {
    if (MessageDebugHelpers.isSameAddresses(TAG, this.address, msg.getAddress())) {
      msg.callHandler(this);
    }
  }

  @Override
  public void onDeviceMessageTimeout(int msgId) {
    MessageDebugHelpers.debugOnMessageTimeout(TAG, address, msgId);
  }

  @Override
  public InetAddress getAddress() {
    return address;
  }

  @Override
  public PlcResource getChild(String[] path) {
    return root.getChild(path, 0);
  }

  @Override
  public PlcResource getRoot() {
    return root;
  }

  @Override
  public void remoteSetAsync(String path, List<String> parameters) {
    MessageDebugHelpers.setResource(TAG, path, parameters, root);
  }

  @Override
  public void remoteUpdateAll() {
    root.remoteGetAll();
  }

}
