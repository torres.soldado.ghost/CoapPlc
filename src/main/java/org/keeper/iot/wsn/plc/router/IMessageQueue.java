package org.keeper.iot.wsn.plc.router;

import org.keeper.iot.wsn.plc.messages.Message;

/**
 * Message queuer interface.
 */
public interface IMessageQueue {
  /**
   * Method addMessage.
   * @param message Message
   */
  public void addMessage(Message message);

  /**
   * Method notifyMessageReceived.
   * @param message Message
   */
  public void notifyMessageReceived(Message message);
}
