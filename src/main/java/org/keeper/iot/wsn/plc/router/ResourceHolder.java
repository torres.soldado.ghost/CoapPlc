/**
 *  Each Plc device (also referred to as a resource) can only attend a single request at a time. This
 * class keeps tabs on resources by allocating and freeing them. A queue is set up for every device
 * in order to serialise the messages sent to it. A {@link Message} includes the message id of the
 * response message. This id along with the device's {@link InetAddress} is used to match a response
 * message.
 * 
 * <p>
 * Resource allocation is done by client request. Resource deallocation is done either when a
 * request's response arrives or due to a timeout. A {@link IPlcMessageListener} either receives an
 * actual response message or an indication that there was a timeout along with the message id of
 * the response message in fault.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.router;

import static com.esotericsoftware.minlog.Log.debug;
import static com.esotericsoftware.minlog.Log.trace;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.observer.Key;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class ResourceHolder implements IMessageQueue {
  /**
   * Tag used by logger.
   */
  private static final String LOG_TAG = ResourceHolder.class.getSimpleName();

  /**
   * Messages that are queued and waiting to be dispatched.
   */
  private final Map<Key, Resource> messagesAwaitingResource = new ConcurrentHashMap<Key, Resource>(
      16, 0.75f);
  
  /**
   * To send messages through.
   */
  private IRouter router;

  /**
   * Constructor for ResourceHolder.
   * 
   * @param router IRouter
   */
  public ResourceHolder(IRouter router) {
    trace(LOG_TAG, "constructor");
    if (null == router) {
      throw new IllegalArgumentException("Router can't be null");
    }
    this.router = router;
  }

  /**
   * Constructor for ResourceHolder.
   */
  @SuppressWarnings("unused")
  private ResourceHolder() {}

  /**
   * Method addMessage.
   * 
   * @param message Message
   * 
   * @see org.keeper.iot.wsn.plc.router.IMessageQueue#addMessage(Message)
   */
  @Override
  public void addMessage(Message message) {
    final Key key = new Key(message.getAddress());
    Resource value = messagesAwaitingResource.get(key);
    if (null == value) {
      debug(LOG_TAG, "Creating new queue for " + message.getAddress().getHostAddress());
      value = new Resource(router);
      messagesAwaitingResource.put(key, value);
    }
    value.add(message);
  }

  /**
   * Method notifyMessageReceived.
   * 
   * @param message Message
   * 
   * @see org.keeper.iot.wsn.plc.router.IMessageQueue#notifyMessageReceived(Message)
   */
  @Override
  public void notifyMessageReceived(Message message) {
    final Resource observer = messagesAwaitingResource.get(new Key(message.getAddress()));
    if (null != observer) {
      observer.notifyArrival(message.getId());
    }
  }

}
