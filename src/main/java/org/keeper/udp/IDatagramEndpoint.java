/**
 * Datagram endpoint interface.
 */

package org.keeper.udp;

import java.net.DatagramPacket;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public interface IDatagramEndpoint {
  /**
   * Send datagram packet.
   * @param packet Datagram packet to send.
   */
  void sendPacket(DatagramPacket packet);
}
