/**
 * Callback handler that received a udp packet.
 * @author sergio.soldado@withus.pt
 */

package org.keeper.udp;

import java.net.DatagramPacket;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public interface IDatagramListener {
  /**
   * Method onDatagram.
   * @param packet DatagramPacket
   */
  void onDatagram(DatagramPacket packet);
}
