package org.keeper.iot.wsn.plc.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.handler.resources.ResourceNode;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsSetAttributeRequest;
import org.keeper.iot.wsn.plc.observer.IObserver;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class AppDeviceResourceHolderTest {

  class CustomCore implements ICore {
    Message lastMessageSent = null;
    
    public CustomCore() {
    }

    @Override
    public void subscribe(IObserver obs, InetAddress addr, int msgId) {
    }

    @Override
    public void subscribe(IObserver obs, int msgId) {
    }

    @Override
    public void unsubscribe(IObserver obs, InetAddress addr, int msgId) {
    }

    @Override
    public void unsubscribe(IObserver obs, int msgId) {
    }

    @Override
    public void send(Message msg) {
      lastMessageSent = msg;
    }

    @Override
    public void sendAndSubscribeOneShot(IObserver obs, Message msg) {
    }

  }

  /**
   * Test basic facilities of {@link IResourceHandler} interface.
   */
  @SuppressWarnings("unused")
  @Test
  public void testDefaultResources() {
    CustomCore core = new CustomCore();

    InetAddress address = null;
    try {
      address = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    IResourceHandler rH = new HandlerAppDevice(core, address, (byte) 0);

    // test getAddress()
    assertEquals(address, rH.getAddress());

    // test getResourceTree()
    ResourceNode root = rH.getRootNode();
    assertEquals("device", root.name);
    assertNull(root.value);
    assertNotNull(root.children);

    // test toString()
    System.out.println(root); // just for visualisation

    // test getResource()
    ResourceNode version = rH.getResource("version");
    assertEquals("N/A", version.value);
    ResourceNode customID = rH.getResource("customId");
    assertEquals("N/A", version.value);
  }

  // TODO test onDeviceMessage
  
  @Test
  public void testSetResource() {
    CustomCore core = new CustomCore();

    InetAddress address = null;
    try {
      address = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    IResourceHandler rH = new HandlerAppDevice(core, address, (byte) 0);
    assertFalse(rH.executeResourceAction("something", "value"));
    assertTrue(rH.executeResourceAction("customId", "something"));
    AppGenericsSetAttributeRequest msg = (AppGenericsSetAttributeRequest)core.lastMessageSent;
    byte[] attribute = AppGenericsSetAttributeRequest.getAttributeData(msg.getRaw());
    System.out.println(msg);
    assertEquals(new String("something"), new String(attribute));
  }
}
