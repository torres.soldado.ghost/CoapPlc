package org.keeper.iot.wsn.plc.messages.appdevice;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;

import static org.junit.Assert.*;

import com.esotericsoftware.minlog.Log;

import org.junit.BeforeClass;
import org.junit.Test;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class AppDeviceGetVersionResponseTest {
  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Log.set(LEVEL_TRACE);
  }

  class CustomHandler implements AppDevice {
    public byte version = 0;
    public int status = 0;
    public InetAddress address = null;
    public byte endpoint = 0;
    public byte sequenceNumber = 0;

    @Override
    public void onAppDeviceGetVersionResponse(InetAddress address, byte endpoint, int status,
        byte version, byte sequenceNumber) {
      this.address = address;
      this.endpoint = endpoint;
      this.status = status;
      this.version = version;
      this.sequenceNumber = sequenceNumber;
    }

  }

  @Test
  public void test() {
    String tag = "test";
    
    InetAddress address = null;
    try {
      address = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    byte endpoint = 34;
    byte version = 123;
    byte seqNum = (byte) 145;
    byte[] msgDataRaw =
        {0, 0,
        MessageFields.SOF,
        MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_APP,
        MessageFields.APP_CLUSTER_DEVICE,
        AppDevice.DEV_CMD_GET_VERSION,
        endpoint,
        1, 0, 0, 0,
        version,
        seqNum,
        0};
    MessageFields.setChecksumAuto(msgDataRaw);

    Message msg = new AppDeviceGetVersionResponse(address, msgDataRaw);

    assertTrue(MessageDebugHelpers.isSameAddresses(tag, address, msg.getAddress()));

    CustomHandler handler = new CustomHandler();
    msg.callHandler(handler);
    assertTrue(MessageDebugHelpers.isSameAddresses(tag, address, msg.getAddress()));
    assertEquals(endpoint, handler.endpoint);
    assertEquals(version, handler.version);
    assertEquals(1, handler.status);
    assertEquals(seqNum, handler.sequenceNumber);
  }

}
