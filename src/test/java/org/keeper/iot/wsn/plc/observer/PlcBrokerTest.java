/**
 * 
 */
package org.keeper.iot.wsn.plc.observer;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import com.esotericsoftware.minlog.Log;

import org.junit.BeforeClass;
import org.junit.Test;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.dummy.Dummy;
import org.keeper.iot.wsn.plc.messages.dummy.DummyResponseMessage;
import org.keeper.utils.Helpers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author keeper
 *
 */
public class PlcBrokerTest {
  @BeforeClass
  public static void oneTimeSetUp() {
    Log.set(LEVEL_TRACE);
  }

  class CustomObserver implements IObserver, Dummy {
    public boolean messageReceived = false;
    public boolean timeout = false;
    public boolean dummyHandlerCalled = false;
    public String name = null;

    public CustomObserver(String name) {
      this.name = name;
    }

    @Override
    public void onDeviceMessage(Message msg) {
      messageReceived = true;
      msg.callHandler(this);
    }

    @Override
    public void onDeviceMessageTimeout(int msgId) {
      timeout = true;
    }

    @Override
    public void onDummyResponse(InetAddress address) {
      dummyHandlerCalled = true;
    }

  }

  /**
   * Test method for
   * {@link org.keeper.iot.wsn.plc.observer.PlcBroker#onPlcMessage(org.keeper.iot.wsn.plc.messages.Message)}
   * .
   */
  @Test
  public void testonPlcMessage() {
    ArrayList<CustomObserver> obs1 = new ArrayList<CustomObserver>();
    for (int i = 0; i < 10; ++i) {
      obs1.add(new CustomObserver("obs1_" + Integer.toString(i)));
    }

    PlcBroker broker = new PlcBroker();
    for (int i = 0; i < obs1.size(); ++i) {
      broker.registerObserver(obs1.get(i), DummyResponseMessage.REGISTRATION_ID, false);
    }

    for (int i = 0; i < obs1.size(); ++i) {
      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
      assertEquals(false, obs1.get(i).messageReceived);
      assertEquals(false, obs1.get(i).timeout);
      assertEquals(false, obs1.get(i).dummyHandlerCalled);
    }

    Message m = null;
    try {
      m = DummyResponseMessage.create(InetAddress.getByName("192.168.1.100"));
    } catch (UnknownHostException e) {
      fail("Shouldn't happen");
      e.printStackTrace();
    }

    broker.onPlcMessage(m);

    for (int i = 0; i < obs1.size(); ++i) {
      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
      assertEquals(true, obs1.get(i).messageReceived);
      assertEquals(false, obs1.get(i).timeout);
      assertEquals(true, obs1.get(i).dummyHandlerCalled);
    }
  }

  /**
   * Concurrency test for {@link org.keeper.iot.wsn.plc.observer.PlcBroker#observerMap}
   */
   @Test
   public void testonConcurrency() {
   final ArrayList<CustomObserver> obs1 = new ArrayList<CustomObserver>();
   final PlcBroker broker = new PlcBroker();
   for (int i = 0; i < 5; ++i) {
   obs1.add(new CustomObserver("obs1_" + Integer.toString(i)));
   }
  
   class Register implements Runnable {
   public boolean keepWorking = true;
  
   @Override
   public void run() {
   while (keepWorking) {
   int i = Helpers.getRandomInt(0, obs1.size() - 1);
   broker.registerObserver(obs1.get(i), DummyResponseMessage.REGISTRATION_ID, false);
   }
   }
   }
  
   class Unregister implements Runnable {
   public boolean keepWorking = true;
  
   @Override
   public void run() {
   while (keepWorking) {
   int i = Helpers.getRandomInt(0, obs1.size() - 1);
   broker.removeObserver(obs1.get(i), DummyResponseMessage.REGISTRATION_ID);
   }
   }
   }
  
   Register reger = new Register();
   Unregister unreger = new Unregister();
   Thread regThread = new Thread(reger, "reger");
   Thread unregThread = new Thread(unreger, "unreger");
   regThread.start();
   unregThread.start();
  
   try {
   Thread.sleep(3000);
   } catch (InterruptedException e) {
   e.printStackTrace();
   fail("Should not happen");
   }
  
   reger.keepWorking = false;
   unreger.keepWorking = false;
  
   try {
   regThread.join();
   unregThread.join();
   } catch (InterruptedException e) {
   e.printStackTrace();
   fail("Should not happen");
   }
  
   }

  /**
   * You must not be able to registering the same observer for the same key more than once.
   */
  @SuppressWarnings("unchecked")
  @Test
  public void testObserverMultipleRegistrations() {
    @SuppressWarnings("rawtypes")
    Class c = null;
    Method registerObs = null;
    Field obsMap = null;
    Object broker = null;
    Map<Key, LinkedList<ObserverEntry>> observerMap = null;
    try {
      c = Class.forName("org.keeper.iot.wsn.plc.observer.PlcBroker");
      registerObs =
          c.getDeclaredMethod("registerObserver", new Class[] {IObserver.class, int.class,
              boolean.class});
      obsMap = c.getDeclaredField("observerMap");
      obsMap.setAccessible(true);
      broker = c.newInstance();
      observerMap = (Map<Key, LinkedList<ObserverEntry>>) obsMap.get(broker);
    } catch (NoSuchMethodException e1) {
      fail("Shouldn't happen");
      e1.printStackTrace();
    } catch (SecurityException e1) {
      fail("Shouldn't happen");
      e1.printStackTrace();
    } catch (ClassNotFoundException e) {
      fail("Shouldn't happen");
      e.printStackTrace();
    } catch (NoSuchFieldException e) {
      fail("Shouldn't happen");
      e.printStackTrace();
    } catch (InstantiationException e) {
      fail("Shouldn't happen");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      fail("Shouldn't happen");
      e.printStackTrace();
    }

    CustomObserver obs = new CustomObserver("Observer");
    assertEquals(false, obs.messageReceived);
    assertEquals(false, obs.timeout);
    assertEquals(false, obs.dummyHandlerCalled);

    try {
      Key key = new Key(DummyResponseMessage.REGISTRATION_ID);
      List<ObserverEntry> obsList = observerMap.get(key);
      assertNull(obsList);
      
      registerObs.invoke(broker, new Object[] {obs, DummyResponseMessage.REGISTRATION_ID, false});
      
      obsList = observerMap.get(key);
      int entriesFound = 0;
      for (ObserverEntry e : obsList) {
        if (e.obs.equals(obs)) {
          ++entriesFound;
        }
      }
      assertEquals(1, entriesFound);
      
      registerObs.invoke(broker, new Object[] {obs, DummyResponseMessage.REGISTRATION_ID, false});
      
      entriesFound = 0;
      for (ObserverEntry e : obsList) {
        if (e.obs.equals(obs)) {
          ++entriesFound;
        }
      }
      assertEquals(1, entriesFound);
    } catch (IllegalAccessException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    } catch (InvocationTargetException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.observer.PlcBroker#onPlcMessageTimeout(int)}.
   */
  @Test
  public void testonPlcMessageTimeout() {
    ArrayList<CustomObserver> obs1 = new ArrayList<CustomObserver>();
    for (int i = 0; i < 10; ++i) {
      obs1.add(new CustomObserver("obs1_" + Integer.toString(i)));
    }

    PlcBroker broker = new PlcBroker();
    for (int i = 0; i < obs1.size(); ++i) {
      broker.registerObserver(obs1.get(i), DummyResponseMessage.REGISTRATION_ID, false);
    }

    for (int i = 0; i < obs1.size(); ++i) {
      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
      assertEquals(false, obs1.get(i).messageReceived);
      assertEquals(false, obs1.get(i).timeout);
      assertEquals(false, obs1.get(i).dummyHandlerCalled);
    }

    Message m = null;
    try {
      m = DummyResponseMessage.create(InetAddress.getByName("192.168.1.100"));
    } catch (UnknownHostException e) {
      fail("Shouldn't happen");
      e.printStackTrace();
    }

    broker.onPlcMessageTimeout(m);

    for (int i = 0; i < obs1.size(); ++i) {
      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
      assertEquals(false, obs1.get(i).messageReceived);
      assertEquals(true, obs1.get(i).timeout);
      assertEquals(false, obs1.get(i).dummyHandlerCalled);
    }

    for (int i = 0; i < obs1.size(); ++i) {
      obs1.get(i).messageReceived = false;
      obs1.get(i).timeout = false;
      obs1.get(i).dummyHandlerCalled = false;
    }

  }

  /**
   * Test method for
   * {@link org.keeper.iot.wsn.plc.observer.PlcBroker#registerObserver(org.keeper.iot.wsn.plc.observer.IObserver, java.net.InetAddress, int, boolean)}
   * .
   */
//  @Test
//  public void testRegisterObserverIObserverInetAddressIntBoolean() {
//    Message m = null;
//    try {
//      m = DummyResponseMessage.create(InetAddress.getByName("192.168.1.100"));
//    } catch (UnknownHostException e) {
//      fail("Shouldn't happen");
//      e.printStackTrace();
//    }
//
//    Message m2 = null;
//    try {
//      m2 = DummyResponseMessage.create(InetAddress.getByName("192.168.1.101"));
//    } catch (UnknownHostException e) {
//      fail("Shouldn't happen");
//      e.printStackTrace();
//    }
//
//    ArrayList<CustomObserver> obs1 = new ArrayList<CustomObserver>();
//    for (int i = 0; i < 10; ++i) {
//      obs1.add(new CustomObserver("obs1_" + Integer.toString(i)));
//    }
//
//    ArrayList<CustomObserver> obsWithAddr = new ArrayList<CustomObserver>();
//    for (int i = 0; i < 20; ++i) {
//      obsWithAddr.add(new CustomObserver("obsWithAddr_" + Integer.toString(i)));
//    }
//
//    ArrayList<CustomObserver> obsWithAddr2 = new ArrayList<CustomObserver>();
//    for (int i = 0; i < 50; ++i) {
//      obsWithAddr2.add(new CustomObserver("obsWithAddr2_" + Integer.toString(i)));
//    }
//
//    PlcBroker broker = new PlcBroker();
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      broker.registerObserver(obsWithAddr.get(i), m.getAddress(), m.getId(), false);
//    }
//    for (int i = 0; i < obsWithAddr2.size(); ++i) {
//      broker.registerObserver(obsWithAddr2.get(i), m2.getAddress(), m2.getId(), true); // OneShot!!!
//    }
//    for (int i = 0; i < obs1.size(); ++i) {
//      broker.registerObserver(obs1.get(i), DummyResponseMessage.REGISTRATION_ID, false);
//    }
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
//      assertEquals(false, obs1.get(i).messageReceived);
//      assertEquals(false, obs1.get(i).timeout);
//      assertEquals(false, obs1.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      assertEquals("obsWithAddr_" + Integer.toString(i), obsWithAddr.get(i).name);
//      assertEquals(false, obsWithAddr.get(i).messageReceived);
//      assertEquals(false, obsWithAddr.get(i).timeout);
//      assertEquals(false, obsWithAddr.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obsWithAddr2.size(); ++i) {
//      assertEquals("obsWithAddr2_" + Integer.toString(i), obsWithAddr2.get(i).name);
//      assertEquals(false, obsWithAddr2.get(i).messageReceived);
//      assertEquals(false, obsWithAddr2.get(i).timeout);
//      assertEquals(false, obsWithAddr2.get(i).dummyHandlerCalled);
//    }
//
//    broker.onPlcMessage(m);
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
//      assertEquals(true, obs1.get(i).messageReceived);
//      assertEquals(false, obs1.get(i).timeout);
//      assertEquals(true, obs1.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      assertEquals("obsWithAddr_" + Integer.toString(i), obsWithAddr.get(i).name);
//      assertEquals(true, obsWithAddr.get(i).messageReceived);
//      assertEquals(false, obsWithAddr.get(i).timeout);
//      assertEquals(true, obsWithAddr.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obsWithAddr2.size(); ++i) {
//      assertEquals("obsWithAddr2_" + Integer.toString(i), obsWithAddr2.get(i).name);
//      assertEquals(false, obsWithAddr2.get(i).messageReceived);
//      assertEquals(false, obsWithAddr2.get(i).timeout);
//      assertEquals(false, obsWithAddr2.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      obs1.get(i).messageReceived = false;
//      obs1.get(i).timeout = false;
//      obs1.get(i).dummyHandlerCalled = false;
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      obsWithAddr.get(i).messageReceived = false;
//      obsWithAddr.get(i).timeout = false;
//      obsWithAddr.get(i).dummyHandlerCalled = false;
//    }
//
//    broker.onPlcMessage(m2);
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
//      assertEquals(true, obs1.get(i).messageReceived);
//      assertEquals(false, obs1.get(i).timeout);
//      assertEquals(true, obs1.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      assertEquals("obsWithAddr_" + Integer.toString(i), obsWithAddr.get(i).name);
//      assertEquals(false, obsWithAddr.get(i).messageReceived);
//      assertEquals(false, obsWithAddr.get(i).timeout);
//      assertEquals(false, obsWithAddr.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obsWithAddr2.size(); ++i) {
//      assertEquals("obsWithAddr2_" + Integer.toString(i), obsWithAddr2.get(i).name);
//      assertEquals(true, obsWithAddr2.get(i).messageReceived);
//      assertEquals(false, obsWithAddr2.get(i).timeout);
//      assertEquals(true, obsWithAddr2.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      obs1.get(i).messageReceived = false;
//      obs1.get(i).timeout = false;
//      obs1.get(i).dummyHandlerCalled = false;
//    }
//
//    for (int i = 0; i < obsWithAddr2.size(); ++i) {
//      obsWithAddr2.get(i).messageReceived = false;
//      obsWithAddr2.get(i).timeout = false;
//      obsWithAddr2.get(i).dummyHandlerCalled = false;
//    }
//
//    broker.onPlcMessage(m);
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
//      assertEquals(true, obs1.get(i).messageReceived);
//      assertEquals(false, obs1.get(i).timeout);
//      assertEquals(true, obs1.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      assertEquals("obsWithAddr_" + Integer.toString(i), obsWithAddr.get(i).name);
//      assertEquals(true, obsWithAddr.get(i).messageReceived);
//      assertEquals(false, obsWithAddr.get(i).timeout);
//      assertEquals(true, obsWithAddr.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obsWithAddr2.size(); ++i) {
//      assertEquals("obsWithAddr2_" + Integer.toString(i), obsWithAddr2.get(i).name);
//      assertEquals(false, obsWithAddr2.get(i).messageReceived);
//      assertEquals(false, obsWithAddr2.get(i).timeout);
//      assertEquals(false, obsWithAddr2.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      obs1.get(i).messageReceived = false;
//      obs1.get(i).timeout = false;
//      obs1.get(i).dummyHandlerCalled = false;
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      obsWithAddr.get(i).messageReceived = false;
//      obsWithAddr.get(i).timeout = false;
//      obsWithAddr.get(i).dummyHandlerCalled = false;
//    }
//
//    broker.onPlcMessage(m2);
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
//      assertEquals(true, obs1.get(i).messageReceived);
//      assertEquals(false, obs1.get(i).timeout);
//      assertEquals(true, obs1.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      assertEquals("obsWithAddr_" + Integer.toString(i), obsWithAddr.get(i).name);
//      assertEquals(false, obsWithAddr.get(i).messageReceived);
//      assertEquals(false, obsWithAddr.get(i).timeout);
//      assertEquals(false, obsWithAddr.get(i).dummyHandlerCalled);
//    }
//
//    // because of oneshot should be unregistered
//    for (int i = 0; i < obsWithAddr2.size(); ++i) {
//      assertEquals("obsWithAddr2_" + Integer.toString(i), obsWithAddr2.get(i).name);
//      assertEquals(false, obsWithAddr2.get(i).messageReceived);
//      assertEquals(false, obsWithAddr2.get(i).timeout);
//      assertEquals(false, obsWithAddr2.get(i).dummyHandlerCalled);
//    }
//  }

  /**
   * Test method for
   * {@link org.keeper.iot.wsn.plc.observer.PlcBroker#removeObserver(org.keeper.iot.wsn.plc.observer.IObserver, java.net.InetAddress, int)}
   * .
   */
//  @Test
//  public void testRemoveObserverIObserverInetAddressInt() {
//    ArrayList<CustomObserver> obs1 = new ArrayList<CustomObserver>();
//    for (int i = 0; i < 10; ++i) {
//      obs1.add(new CustomObserver("obs1_" + Integer.toString(i)));
//    }
//
//    PlcBroker broker = new PlcBroker();
//    for (int i = 0; i < obs1.size(); ++i) {
//      broker.registerObserver(obs1.get(i), DummyResponseMessage.REGISTRATION_ID, false);
//    }
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
//      assertEquals(false, obs1.get(i).messageReceived);
//      assertEquals(false, obs1.get(i).timeout);
//      assertEquals(false, obs1.get(i).dummyHandlerCalled);
//    }
//
//    Message m = null;
//    try {
//      m = DummyResponseMessage.create(InetAddress.getByName("192.168.1.100"));
//    } catch (UnknownHostException e) {
//      fail("Shouldn't happen");
//      e.printStackTrace();
//    }
//
//    broker.onPlcMessage(m);
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
//      assertEquals(true, obs1.get(i).messageReceived);
//      assertEquals(false, obs1.get(i).timeout);
//      assertEquals(true, obs1.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      obs1.get(i).messageReceived = false;
//      obs1.get(i).timeout = false;
//      obs1.get(i).dummyHandlerCalled = false;
//    }
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      broker.removeObserver(obs1.get(i), DummyResponseMessage.REGISTRATION_ID);
//    }
//
//    broker.onPlcMessage(m);
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
//      assertEquals(false, obs1.get(i).messageReceived);
//      assertEquals(false, obs1.get(i).timeout);
//      assertEquals(false, obs1.get(i).dummyHandlerCalled);
//    }
//
//    /*
//     * With address
//     */
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      broker.registerObserver(obs1.get(i), DummyResponseMessage.REGISTRATION_ID, false);
//    }
//
//    ArrayList<CustomObserver> obsWithAddr = new ArrayList<CustomObserver>();
//    for (int i = 0; i < 20; ++i) {
//      obsWithAddr.add(new CustomObserver("obsWithAddr_" + Integer.toString(i)));
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      broker.registerObserver(obsWithAddr.get(i), m.getAddress(), m.getId(), false);
//    }
//
//    broker.onPlcMessage(m);
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
//      assertEquals(true, obs1.get(i).messageReceived);
//      assertEquals(false, obs1.get(i).timeout);
//      assertEquals(true, obs1.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      assertEquals("obsWithAddr_" + Integer.toString(i), obsWithAddr.get(i).name);
//      assertEquals(true, obsWithAddr.get(i).messageReceived);
//      assertEquals(false, obsWithAddr.get(i).timeout);
//      assertEquals(true, obsWithAddr.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      obs1.get(i).messageReceived = false;
//      obs1.get(i).timeout = false;
//      obs1.get(i).dummyHandlerCalled = false;
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      obsWithAddr.get(i).messageReceived = false;
//      obsWithAddr.get(i).timeout = false;
//      obsWithAddr.get(i).dummyHandlerCalled = false;
//    }
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      broker.removeObserver(obs1.get(i), DummyResponseMessage.REGISTRATION_ID);
//    }
//
//    broker.onPlcMessage(m);
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
//      assertEquals(false, obs1.get(i).messageReceived);
//      assertEquals(false, obs1.get(i).timeout);
//      assertEquals(false, obs1.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      assertEquals("obsWithAddr_" + Integer.toString(i), obsWithAddr.get(i).name);
//      assertEquals(true, obsWithAddr.get(i).messageReceived);
//      assertEquals(false, obsWithAddr.get(i).timeout);
//      assertEquals(true, obsWithAddr.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      obs1.get(i).messageReceived = false;
//      obs1.get(i).timeout = false;
//      obs1.get(i).dummyHandlerCalled = false;
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      obsWithAddr.get(i).messageReceived = false;
//      obsWithAddr.get(i).timeout = false;
//      obsWithAddr.get(i).dummyHandlerCalled = false;
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      broker.removeObserver(obsWithAddr.get(i), m.getAddress(), m.getId());
//    }
//
//    broker.onPlcMessage(m);
//
//    for (int i = 0; i < obs1.size(); ++i) {
//      assertEquals("obs1_" + Integer.toString(i), obs1.get(i).name);
//      assertEquals(false, obs1.get(i).messageReceived);
//      assertEquals(false, obs1.get(i).timeout);
//      assertEquals(false, obs1.get(i).dummyHandlerCalled);
//    }
//
//    for (int i = 0; i < obsWithAddr.size(); ++i) {
//      assertEquals("obsWithAddr_" + Integer.toString(i), obsWithAddr.get(i).name);
//      assertEquals(false, obsWithAddr.get(i).messageReceived);
//      assertEquals(false, obsWithAddr.get(i).timeout);
//      assertEquals(false, obsWithAddr.get(i).dummyHandlerCalled);
//    }
//  }

}
